#!/usr/bin/env python3

from ..inputs.input_day1 import INPUT

def _get_triples(_input):
    res = []
    for a in range(len(_input[:-2])):
        res.append(
            sum(
                (_input[a], _input[a+1], _input[a+2])
            )
        )
    return res

def _get_pairs(_input):
    triples = _get_triples(_input)
    for a in range(len(triples[:-1])):
        yield (triples[a], triples[a+1])

def has_increased(pair):
    return pair[0] < pair[1]

if __name__ == "__main__":
    print(sum(map(has_increased, _get_pairs(INPUT))))
